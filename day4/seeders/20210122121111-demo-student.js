"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     */
    await queryInterface.bulkInsert(
      "Students",
      [
        {
          name: "Tri Agung Prasetio",
          email: "test@gmail.com",
          password: "agung123",
          birthday: "1998-07-19",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Bayu Suryo Aji",
          email: "test@gmail.com",
          password: "suryo123",
          birthday: "1992-07-19",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Tamam Ahda Septiadi",
          email: "test@gmail.com",
          password: "tamam123",
          birthday: "1997-07-19",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     */
    await queryInterface.bulkDelete("Students", null, {});
  },
};
