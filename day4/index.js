const express = require("express");
const app = express();
const db = require("./models");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.post("/", async (req, res) => {
  const name = req.body["name"];
  const email = req.body["email"];
  const password = req.body["password"];
  const birthday = req.body["birthday"];

  await db.Students.create({
    name: name,
    email: email,
    password: password,
    birthday: birthday,
  });

  res.json({ massage: "Succes insert data" });
});

app.get("/", async (req, res) => {
  const data = await db.Students.findAll();
  res.json(data);
});

app.put("/", async (req, res) => {
  const name = req.body["name"];
  const email = req.body["email"];
  const password = req.body["password"];
  const birthday = req.body["birthday"];
  const id = req.body.id;

  await db.Students.update(
    {
      name: name,
      email: email,
      password: password,
      birthday: birthday,
    },
    { where: { id: id } }
  );
  res.json({ massage: `Succes ${id}` });
});

app.delete("/", async (req, res) => {
  const id = req.query.id;
  await db.Students.destroy({ where: { id: id } });
  res.json({ massage: `Select delete data from users with id ${id}` });
});

app.listen(3000, () => console.log("app running on port 3000"));
