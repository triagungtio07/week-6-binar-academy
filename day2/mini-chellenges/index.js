const express = require('express')
const app = express()

const data = [{
        "name": "Belgian Waffles",
        "price": "$5.95",
        "description": "Two of our famous Belgian Waffles with plenty of real maple syrup",
        "calories": "650"
    },
    {
        "name": "Strawberry Belgian Waffles",
        "price": "$7.95",
        "description": "Light Belgian waffles covered with strawberries and whipped cream",
        "calories": "900"
    },
    {
        "name": "Berry-Berry Belgian Waffles",
        "price": "$8.95",
        "description": "Belgian waffles covered with assorted fresh berries and whipped cream",
        "calories": "900"
    },
    {
        "name": "French Toast",
        "price": "$4.50",
        "description": "Thick slices made from our homemade sourdough bread",
        "calories": "600"
    },
    {
        "name": "Homestyle Breakfast",
        "price": "$6.95",
        "description": "Two eggs, bacon or sausage, toast, and our ever-popular hash browns",
        "calories": "950"
    }
]

app.get('/all', function (req, res) {
    res.json(data)
})

app.get('/food/:index', function (req, res) {
    
    res.json(data[req.params.index-1])
})

app.get('/food2', function (req, res) {
    
    res.json(req.query.data)
})

app.get('/calories', function (req, res) {
    const total = (parseInt(data[0]["calories"]))+(parseInt(data[1]["calories"]))+(parseInt(data[2]["calories"]))+(parseInt(data[3]["calories"]))+(parseInt(data[4]["calories"]))
    const calories = {total_calories: total}
    res.json(calories)
})

app.listen(3000, () => console.log("app running on port 3000"))