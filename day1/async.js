//Contoh 1

// const fungsi1 = () => {
//     setTimeout(() => {
//         console.log('ini pesan pertama')
//     }, 1000)
// }

// const fungsi2 = () => {
//     console.log('ini pesan kedua')
// }

// fungsi1()
// fungsi2()

//Contoh 2

const getNumber = () => {
    setTimeout(()=> {
        return 10;
    }, 1000)
}

const calcNumber = () => {
    const number = getNumber();
    console.log(number**2)
}

calcNumber();
