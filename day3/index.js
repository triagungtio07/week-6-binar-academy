const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const db = require("./models");

app.use(bodyParser.json());

const checkDataTypes = (req, res, next) => {
  const parseUsername = Number.isNaN(parseInt(req.query.username))
    ? ""
    : parseInt(req.query.username);
  if (
    typeof parseUsername == "string" &&
    typeof req.query.password == "string"
  ) {
    next();
  } else {
    return res.send("data username dan password tidak sesuai dengan tipe data");
  }
};

const checkRequestField = (req, res, next) => {
  //validasi untuk mengecek data request
  if (req.query.username && req.query.password) {
    next();
  } else {
    return res.send("data username dan password tidak ada");
  }
};
const checkLogin = async (req, res, next) => {
  const dataCheck = await db.users.findOne({
    where: { username: req.query.username, password: req.query.password },
  });
  if (dataCheck) {
    next();
  } else {
    return res.send("kamu tidak terdaftar di tabel users");
  }
};
app.get("/", checkRequestField, checkDataTypes, checkLogin, (req, res) =>
  res.send("kamu telah terdaftar di tabel users")
);

app.get("*", (req, res) => {
  res.status(404).json({ message: "Halaman yang anda cari tidak ditemukan" });
});

const checkCreateField = async (req, res, next) => {
  if (
    req.body["username"] !== "" &&
    req.body["password"] !== "" &&
    req.body["age"] !== null &&
    req.body["email"] !== ""
  ) {
    next();
  } else {
    return res.send("Lengkapi data anda");
  }
};

const checkAgeData = async (req, res, next) => {
  if (typeof req.body["age"] == "number") {
    next();
  } else {
    return res.send("Data umur salah");
  }
};

app.post("/", checkCreateField, checkAgeData, async (req, res) => {
  const username = req.body["username"];
  const password = req.body["password"];
  const age = req.body["age"];
  const email = req.body["email"];
  await db.users.create({
    username: username,
    password: password,
    age: age,
    email: email,
    createdAt: new Date(),
    updatedAt: new Date(),
  });
  res.send("sukses isi data");
});

app.listen(3000, () => console.log("app running on port 3000"));
